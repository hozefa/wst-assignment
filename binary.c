#include<stdio.h>
#include<math.h>
void bin(int x);
void bin(int x) {
    for(int i = 0; i < pow(2, x); i++) {
        int mask = pow(2, x);
        mask = mask >> 1;
        printf("%d\t", i);
        while(mask > 0) {
            if((mask & i) == 0)
                printf("0");
            else
                printf("1");
            mask = mask >> 1;
        }
        printf("\n");
    }
}
int main() {
    int a;
    printf("Enter limit: ");
    if (scanf("%d", &a) == 1) {
    	bin(a);
    }
    else {
    	printf("Please enter a valid number!\n");
    }
}